package com.devcamp.api.j58jpahibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan({"com.edu", "com.abc"})
@SpringBootApplication //(scanBasePackages = {"com.edu", "com.abc"})
public class J58jpahibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(J58jpahibernateApplication.class, args);
	}

}
